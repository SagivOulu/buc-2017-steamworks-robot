function checkcart(){
var Path = window.location.pathname; 
if (Path.substring(Path.lastIndexOf('/')).toLowerCase() == '/shoppingcart.asp') {


$("[id^='Quantity']").each(function(){
$(this).data('default2', $(this).val());
});
$("[name='Proceed_To_Checkout_Form']").submit(function(){
var hasChanged = false;
$("[id^='Quantity']").each(function(){
if($(this).val() != $(this).data('default2')){
hasChanged = true;
}
});
if(hasChanged){
alert("We just noticed that the quantities of one or more items in your cart has changed!\nYour cart will be recalculated then you can proceed to checkout.");
$('#btnRecalculate').trigger('click');
return false;
}
});
}
};

function ShippingMethod_OnChange()
{
  if ( $('select[name=ShippingSpeedChoice]').val() == "102") {
    $("#rates-unavailable-message").show("blind", {}, 700, null)
  } else {
    $("#rates-unavailable-message").hide()
  }


  if ( $('select[name=ShippingSpeedChoice]').val() == "1337") {
    $("#expresssaver-message").show("blind", {}, 700, null)
  } else {
    $("#expresssaver-message").hide()
  }
  

  if ( $('select[name=ShippingSpeedChoice]').val() == "504") {
    $("#twodayshipping-message").show("blind", {}, 700, null)
  } else {
    $("#twodayshipping-message").hide()
   }


  if ( $('select[name=ShippingSpeedChoice]').val() == "505") {
    $("#standardovernight-message").show("blind", {}, 700, null)
  } else {
    $("#standardovernight-message").hide()
  }
  

  if ( $('select[name=ShippingSpeedChoice]').val() == "506") {
    $("#priorityovernight-message").show("blind", {}, 700, null)
  } else {
    $("#priorityovernight-message").hide()
   }
  

  if ( $('select[name=ShippingSpeedChoice]').val() == "1031") {
    $("#MichiganStates").show("blind", {}, 700, null)
  } else {
    $("#MichiganStates").hide()
   }
  

  if ( $('select[name=ShippingSpeedChoice]').val() == "590") {
    $("#AndyMarkPitChamps").show("blind", {}, 700, null)
  } else {
    $("#AndyMarkPitChamps").hide()
   }

 if ( $('select[name=ShippingSpeedChoice]').val() == "550" ||  $('select[name=ShippingSpeedChoice]').val() == "551" ||  $('select[name=ShippingSpeedChoice]').val() == "552" ||  $('select[name=ShippingSpeedChoice]').val() == "553" ||  $('select[name=ShippingSpeedChoice]').val() == "554" || $('select[name=ShippingSpeedChoice]').val() == "560" ||  $('select[name=ShippingSpeedChoice]').val() == "561" ||  $('select[name=ShippingSpeedChoice]').val() == "562" ||  $('select[name=ShippingSpeedChoice]').val() == "563" ||  $('select[name=ShippingSpeedChoice]').val() == "564") {
    $("#groundServiceMap").show("fold", {}, 700, null)
  } else {
    $("#groundServiceMap").hide()
  }

if ( $('select[name=ShippingSpeedChoice]').val() == "808" ||  $('select[name=ShippingSpeedChoice]').val() == "810" ||  $('select[name=ShippingSpeedChoice]').val() == "813") {
     $("#USPSmessage").dialog({modal: true,buttons: {OK: function() {$(this).dialog( "close" );}}});
  } 

setTimeout(function() { OtherChecks(); }, 1000);
//setTimeout(function() { ShippingPOBox(); }, 2000);
};

function OtherChecks() {
	if ( $.trim( $('#TotalsSHTD').html() ).length > 0){
	var shipcurrTotals = document.getElementById('TotalsSHTD').firstChild.data;
	var shipTotalInNum = Number(shipcurrTotals.replace(/[^0-9\.]+/g,""));
	if (shipTotalInNum >= 500) {
		$("#highshippingcostmessage").dialog({modal: true,buttons: {OK: function() {$(this).dialog( "close" );}}});
	}
	
	var shipToadd1Data = document.getElementById('v65-onepage-shipaddr1').value;
	if (shipToadd1Data.indexOf("PO Box") != -1) {
		if ( $('select[name=ShippingSpeedChoice]').val() == "504" || $('select[name=ShippingSpeedChoice]').val() == "505" || $('select[name=ShippingSpeedChoice]').val() == "506" || $('select[name=ShippingSpeedChoice]').val() == "550" ||  $('select[name=ShippingSpeedChoice]').val() == "551" ||  $('select[name=ShippingSpeedChoice]').val() == "552" ||  $('select[name=ShippingSpeedChoice]').val() == "553" ||  $('select[name=ShippingSpeedChoice]').val() == "554" || $('select[name=ShippingSpeedChoice]').val() == "560" ||  $('select[name=ShippingSpeedChoice]').val() == "561" ||  $('select[name=ShippingSpeedChoice]').val() == "562" ||  $('select[name=ShippingSpeedChoice]').val() == "563" ||  $('select[name=ShippingSpeedChoice]').val() == "564" || $('select[name=ShippingSpeedChoice]').val() == "1337") {
			$("#NoFedExPOBox").dialog({modal: true,buttons: {OK: function() {$(this).dialog( "close" );}}});
		}
	}
	}
	
	
};

function ActivateCustomTableTab(TabPillNumber) {
	$('#menu' + TabPillNumber + 'pill').click()
	$(".nav-pills>li").removeClass("active");
	$('#menu' + TabPillNumber + 'pill').closest("li").addClass("active");
	$(".tab-content>div").removeClass("in active");
	$("#menu" + TabPillNumber).addClass("in active");
}

//doc ready for pages with tables
$( document ).ready(function() {
    var str = window.location.href 
    if (str.indexOf("?") < 0) { return; }
    var parastring = str.substring(str.indexOf("?")+1);
    if (document.getElementById('Header_ProductDetail_TechSpecs_span') != null && parastring.substring(0,4) == 'TECH') {
        ProductDetail_TechSpecs();
        return;
    }
    if (document.getElementById('Header_ProductDetail_ExtInfo_span') != null && parastring.substring(0,4) == 'PICS') {
        ProductDetail_ExtInfo();
        return;
    }
    var navpilllength = $('ul.nav-pills>li').length;
    var paranumber = 0;	    
    if (navpilllength > 0) {
    	    if ((parastring.length == 2) && (!isNaN(parastring.substring(0,2))) ) {
    	    	    paranumber = parastring.substring(0,2);
    	    }
    	    else if ((parastring.length == 1) && (!isNaN(parastring.substring(0,1))) ) {
		    paranumber = parastring.substring(0,1);
	    }
	    else {
	    	    return;
	    }
	    ActivateCustomTableTab(paranumber);
    }
});

$( document ).ready(function() {
	if (document.getElementsByClassName("PageText_L531n").length > 0) {
		$('#CheckOutProceed').hide()
	}
	if (document.getElementsByName("Apply_Payment_Method_To_All_Orders").length > 0) {
		$("input[name='Apply_Payment_Method_To_All_Orders']").attr('checked',false)
		$("input[name='Apply_Payment_Method_To_All_Orders']").attr('value','N')
	}
});

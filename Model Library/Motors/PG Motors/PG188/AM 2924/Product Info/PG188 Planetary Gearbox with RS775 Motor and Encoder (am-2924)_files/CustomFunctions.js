function checkcart(){
var Path = window.location.pathname; 
if (Path.substring(Path.lastIndexOf('/')).toLowerCase() == '/shoppingcart.asp') {


$("[id^='Quantity']").each(function(){
$(this).data('default2', $(this).val());
});
$("[name='Proceed_To_Checkout_Form']").submit(function(){
var hasChanged = false;
$("[id^='Quantity']").each(function(){
if($(this).val() != $(this).data('default2')){
hasChanged = true;
}
});
if(hasChanged){
alert("We just noticed that the quantities of one or more items in your cart has changed!\nYour cart will be recalculated then you can proceed to checkout.");
$('#btnRecalculate').trigger('click');
return false;
}
});
}
};

function ShippingMethod_OnChange()
{
  if ( $('select[name=ShippingSpeedChoice]').val() == "102") {
    $("#rates-unavailable-message").show("blind", {}, 700, null)
  } else {
    $("#rates-unavailable-message").hide()
  }


  if ( $('select[name=ShippingSpeedChoice]').val() == "1337") {
    $("#expresssaver-message").show("blind", {}, 700, null)
  } else {
    $("#expresssaver-message").hide()
  }
  

  if ( $('select[name=ShippingSpeedChoice]').val() == "504") {
    $("#twodayshipping-message").show("blind", {}, 700, null)
  } else {
    $("#twodayshipping-message").hide()
   }


  if ( $('select[name=ShippingSpeedChoice]').val() == "505") {
    $("#standardovernight-message").show("blind", {}, 700, null)
  } else {
    $("#standardovernight-message").hide()
  }
  

  if ( $('select[name=ShippingSpeedChoice]').val() == "506") {
    $("#priorityovernight-message").show("blind", {}, 700, null)
  } else {
    $("#priorityovernight-message").hide()
   }
  

  if ( $('select[name=ShippingSpeedChoice]').val() == "1031") {
    $("#MichiganStates").show("blind", {}, 700, null)
  } else {
    $("#MichiganStates").hide()
   }
  

  if ( $('select[name=ShippingSpeedChoice]').val() == "590") {
    $("#AndyMarkPitChamps").show("blind", {}, 700, null)
  } else {
    $("#AndyMarkPitChamps").hide()
   }

 if ( $('select[name=ShippingSpeedChoice]').val() == "550" ||  $('select[name=ShippingSpeedChoice]').val() == "551" ||  $('select[name=ShippingSpeedChoice]').val() == "552" ||  $('select[name=ShippingSpeedChoice]').val() == "553" ||  $('select[name=ShippingSpeedChoice]').val() == "554" || $('select[name=ShippingSpeedChoice]').val() == "560" ||  $('select[name=ShippingSpeedChoice]').val() == "561" ||  $('select[name=ShippingSpeedChoice]').val() == "562" ||  $('select[name=ShippingSpeedChoice]').val() == "563" ||  $('select[name=ShippingSpeedChoice]').val() == "564") {
    $("#groundServiceMap").show("fold", {}, 700, null)
  } else {
    $("#groundServiceMap").hide()
  }

if ( $('select[name=ShippingSpeedChoice]').val() == "808" ||  $('select[name=ShippingSpeedChoice]').val() == "810" ||  $('select[name=ShippingSpeedChoice]').val() == "813") {
	jAlert("AndyMark will not guarantee delivery with any orders shipped via USPS");
  } 

setTimeout(function() { OtherChecks(); }, 500);
};

function OtherChecks() {
	if ( $.trim( $('#TotalsSHTD').html() ).length > 0){
		var shipcurrTotals = document.getElementById('TotalsSHTD').firstChild.data;
		var shipTotalInNum = Number(shipcurrTotals.replace(/[^0-9\.]+/g,""));
		if (shipTotalInNum >= 500) {
			jAlert("It's possible not all shipping options are shown and may not calculate correctly above $500. Please contact us at 765-868-4779 or <a href='mailto:sales@andymark.com'>sales@andymark.com</a>. You can still put in your order and contact us after to help the best we can. We would like to work with you for the best option and rate for you.");
		}
		
		var shipToadd1Data = '';
		if (document.contains(document.getElementById('v65-onepage-shipaddr1'))) {
			shipToadd1Data = document.getElementById('v65-onepage-shipaddr1').value;
		}
		else if (document.contains(document.getElementById('v65-onepage-billaddr1'))) {
			shipToadd1Data = document.getElementById('v65-onepage-billaddr1').value;			
		}
		if (shipToadd1Data.toUpperCase().indexOf("PO BOX") != -1) {
			if ( $('select[name=ShippingSpeedChoice]').val() == "504" || $('select[name=ShippingSpeedChoice]').val() == "505" || $('select[name=ShippingSpeedChoice]').val() == "506" || $('select[name=ShippingSpeedChoice]').val() == "550" ||  $('select[name=ShippingSpeedChoice]').val() == "551" ||  $('select[name=ShippingSpeedChoice]').val() == "552" ||  $('select[name=ShippingSpeedChoice]').val() == "553" ||  $('select[name=ShippingSpeedChoice]').val() == "554" || $('select[name=ShippingSpeedChoice]').val() == "560" ||  $('select[name=ShippingSpeedChoice]').val() == "561" ||  $('select[name=ShippingSpeedChoice]').val() == "562" ||  $('select[name=ShippingSpeedChoice]').val() == "563" ||  $('select[name=ShippingSpeedChoice]').val() == "564" || $('select[name=ShippingSpeedChoice]').val() == "1337") {
				jAlert("FedEx will not deliver to a PO Box. Please provide a street address.");
			}
		}
	}
	
	
};

function ActivateCustomTableTab(TabPillNumber) {
	$('#menu' + TabPillNumber + 'pill').click()
	$(".nav-pills>li").removeClass("active");
	$('#menu' + TabPillNumber + 'pill').closest("li").addClass("active");
	$(".tab-content>div").removeClass("in active");
	$("#menu" + TabPillNumber).addClass("in active");
}

function CheckBulkInput() {
	var product_qty = $(".v65-productdetail-cartqty").val()
	var product_step = $(".v65-productdetail-cartqty").attr('step');
	if (Number(product_qty) % Number(product_step) != 0) {
		if (Number(product_qty) > 25 && Number(product_qty) < 50) {
			$('input.v65-productdetail-cartqty').attr('value', '25');
		} else {
			var temp_value = (product_qty) - ((product_qty) % (product_step))
			$('input.v65-productdetail-cartqty').attr('value', temp_value);
		}
		jAlert("This product is only available in quantity of 25, or multiples of 50.  The quantity was lowered to the nearest available.");
	}
	if(Number(product_qty) <  50) { $(".v65-productdetail-cartqty").attr('step', '25'); }
	else { $(".v65-productdetail-cartqty").attr('step', '50'); }
}

//doc ready for pages with tables
$( document ).ready(function() {
    var str = window.location.href 
    if (str.indexOf("?") < 0) { return; }
    var parastring = str.substring(str.indexOf("?")+1);
    var parafourchar = parastring.substring(0,4);
    if (document.getElementById('Header_ProductDetail_TechSpecs_span') != null && (parafourchar == 'TECH' || parafourchar == 'DOCS' || parafourchar == 'INFO' || parafourchar == 'MORE')) {
        ProductDetail_TechSpecs();
        return;
    }
    if (document.getElementById('Header_ProductDetail_ExtInfo_span') != null && parafourchar == 'PICS') {
        ProductDetail_ExtInfo();
        return;
    }
    var navpilllength = $('ul.nav-pills>li').length;
    var paranumber = 0;	    
    if (navpilllength > 0) {
    	    if ((parastring.length == 2) && (!isNaN(parastring.substring(0,2))) ) {
    	    	    paranumber = parastring.substring(0,2);
    	    }
    	    else if ((parastring.length == 1) && (!isNaN(parastring.substring(0,1))) ) {
		    paranumber = parastring.substring(0,1);
	    }
	    else {
	    	    return;
	    }
	    ActivateCustomTableTab(paranumber);
    }
});

$( document ).ready(function() {
	if (document.getElementsByClassName("PageText_L531n").length > 0) {
		$('#CheckOutProceed').hide()
	}
	if (document.getElementsByName("Apply_Payment_Method_To_All_Orders").length > 0) {
		$("input[name='Apply_Payment_Method_To_All_Orders']").attr('checked',false)
		$("input[name='Apply_Payment_Method_To_All_Orders']").attr('value','N')
	}
	if ($("input.v65-productdetail-cartqty").length > 0) {
		$("<div id='bulkplaceholder'></div>").insertBefore($("input.v65-productdetail-cartqty"));
		var temp_input_name = $("input.v65-productdetail-cartqty").attr('name');
		$("input.v65-productdetail-cartqty").remove();
		var temp_product_name = $(".productnamecolorLARGE>span").html().toLowerCase().replace(/&amp;/g, "&").replace(/&lt;/g, "<").replace(/&gt;/g, "<").replace(/&quot;/g, "\"").replace(/&apos;/g, "'");
		if (temp_product_name.indexOf('bulk') > -1) {
			$("<input type='number' class='v65-productdetail-cartqty' name='" + temp_input_name + "' step='25' min='0' onkeydown='javascript:QtyEnabledAddToCart();' value='25' onchange='CheckBulkInput()'><br><span class='bulkinputbox'>Bulk Qty Only</span>").insertAfter($("#bulkplaceholder"));
		} else {
			$("<input type='number' class='v65-productdetail-cartqty' name='" + temp_input_name + "' min='0' onkeydown='javascript:QtyEnabledAddToCart();' value='1'>").insertAfter($("#bulkplaceholder"));
		}
		$("#bulkplaceholder").remove();
	}
	if (document.getElementsByClassName("PageText_L332n").length > 0 ) {
		$('#v65-productdetail-action-wrapper table')[0].remove()
	}
});

function addOptionNote ( option, html ) {
    element = document.getElementById( option );
    if (element) {
        parentNode = element.parentNode;
        if ( parentNode ) {
            parentNode.innerHTML = parentNode.innerHTML + '<div class="option-note">' + html + '</div>';
        }
    }
}

function hideOption ( option ) {
    element = document.getElementById( option );
    if (element) {
        parentNode = element.parentNode;
        if ( parentNode ) {
            parentNode.style.display = "none";
            x = parentNode.previousSibling;
            if ( ! x.tagName ) {
                x = x.previousSibling;
            }
            if ( x.tagName == 'DT' ) {
                x.style.display = "none";
            }
        }
    }
}

function showOption ( option ) {
    element = document.getElementById( option );
    if (element) {
        parentNode = element.parentNode;
        if ( parentNode ) {
            parentNode.style.display = "block";
            x = parentNode.previousSibling;
            if ( ! x.tagName ) {
                x = x.previousSibling;
            }
            if ( x.tagName == 'DT' ) {
                x.style.display = "block";
            }
        }
    }
}

function toggleContact( checkbox ) {
    if ( checkbox.checked ) {
        hideOption( 'options_81_text' );
        hideOption( 'options_86_text' );
        hideOption( 'options_91_text' );
    } else {
        // Show contact fields
        showOption( 'options_81_text' );
        showOption( 'options_86_text' );
        showOption( 'options_91_text' );
    }
}

function setToggleContact() {
    x = document.getElementById('options_76_2');
    x.onchange = function(){toggleContact(this);};
}


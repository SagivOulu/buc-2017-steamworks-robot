    var converted=0;
    var json_countries=0;
    var json_regions=0;

    var _regionUpdater = 0;

    function ajaxObject(url, callbackFunction) {
      var that=this;      
      this.updating = false;
      this.abort = function() {
        if (that.updating) {
          that.updating=false;
          that.AJAX.abort();
          that.AJAX=null;
        }
      }
      this.update = function(passData,postMethod) { 
        if (that.updating) { return false; }
        that.AJAX = null;                          
        if (window.XMLHttpRequest) {              
          that.AJAX=new XMLHttpRequest();              
        } else {                                  
          that.AJAX=new ActiveXObject("Microsoft.XMLHTTP");
        }                                             
        if (that.AJAX==null) {                             
          return false;                               
        } else {
          that.AJAX.onreadystatechange = function() {  
            if (that.AJAX.readyState==4) {             
              that.updating=false;                
              that.callback(that.AJAX.responseText,that.AJAX.status,that.AJAX.responseXML);        
              that.AJAX=null;                                         
            }                                                      
          }                                                        
          that.updating = new Date();                              
          if (/post/i.test(postMethod)) {
            var uri=urlCall;
            that.AJAX.open("POST", uri, true);
            that.AJAX.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            that.AJAX.setRequestHeader("Content-Length", passData.length);
            that.AJAX.send(passData);
          } else {
            var uri=urlCall; 
            that.AJAX.open("GET", uri, true);                             
            that.AJAX.send(null);                                         
          }              
          return true;                                             
        }                                                                           
      }
      var urlCall = url;        
      this.callback = callbackFunction || function () { };
    }

    function convertRegionCountry( country_id, region_id, region_message, country_message ) {

        var oRegExp = new RegExp("(^|\\s)required-entry(\\s|$)");

        if (converted) { return; }
        converted = 1;

        var country_iso;
        var country_value;
        var region_value;

        if (!region_message) {
            region_message = '-- Please Select --';
        }
        if (!country_message) {
            country_message = '-- Please Select --';
        }

        country = document.getElementById( country_id );
        if (country.type == 'select-one') {
            country_value = country.options[country.selectedIndex].text;
        } else {
            country_value = country.value;
        }

        ifiCountry = document.createElement('select');
        ifiCountry.id = country_id
        country_name = country_id.split('_');
        if ( country_name[0] == 'options' ) {
            ifiCountry.name = 'options[' +country_name[1]+ ']';
        } else {
            ifiCountry.name = country_id;
        }

        // Check to see if country has the required class.
        if(oRegExp.test(country.getAttribute("class"))) {
            ifiCountry.className = 'required-entry';
        }

        var top_options = {"":country_message,"US":"United States","CA":"Canada","00":"-----"};
        // Add the "pick a country" message.
        for ( x in top_options ) {
            ifiOption = document.createElement('option');
            if (x == 0) {
                ifiOption.value = '';
            } else {
                ifiOption.value = top_options[x];
            }
            ifiOption.text  = top_options[x];

            try {
                ifiCountry.add(ifiOption,null); 
            } catch(ex) {
                ifiCountry.add(ifiOption); 
            }
        }

        // Add the rest of the countries to the select box.
        for (x in json_countries) {

            selected = false;
            ifiOption = document.createElement('option');
            ifiOption.value = json_countries[x];
            ifiOption.text  = json_countries[x];

            if ( country_value == json_countries[x] ) {
                country_iso = x;
                ifiOption.selected = true;
            }

            try {
                ifiCountry.add(ifiOption,null); 
            } catch(ex) {
                ifiCountry.add(ifiOption);
            }
        }
//        country.replace(ifiCountry);
        jQuery('#'+country_id).replaceWith(ifiCountry);
       
        region = document.getElementById( region_id  );
        var region_value = '';
        if (region.type == 'select-one') {
            region_value = regionoptions[region.selectedIndex];
        } else {
            region_value = region.value;
        }

        ifiRegion = document.createElement('select');
        ifiRegion.id = region_id;
        region_name = region_id.split('_');
        if ( region_name[0] == 'options' ) {
            ifiRegion.name = 'options[' +region_name[1]+ ']';
        } else {
            ifiRegion.name = region_id;
        }

        // Check to see if region has the required class.
        if(oRegExp.test(region.getAttribute("class"))) {
            ifiRegion.className = 'required-entry';
        }

        // Add a null option asking for input.
        ifiOption = document.createElement('option');
        ifiOption.value = '';
        ifiOption.text  = region_message;
        try {
            ifiRegion.add(ifiOption,null); // standards compliant
        } catch(ex) {
            ifiRegion.add(ifiOption); // IE only
        }

        if ( json_regions[country_value] ) {
            for ( x in json_regions[country_value] ) {
                ifiOption = document.createElement('option');
                ifiOption.value = json_regions[country_value][x].name;
                ifiOption.text  = json_regions[country_value][x].name;
                if (ifiOption.text == region_value) {
                    ifiOption.selected = true;
                }

                try {
                    ifiRegion.add(ifiOption,null); 
                } catch(ex) {
                    ifiRegion.add(ifiOption); 
                }
            }
        }

//        region.replace(ifiRegion);
        jQuery('#'+region_id).replaceWith(ifiRegion);

        _regionUpdater = new RegionUpdater( country_id, region_id, region_id, json_regions, 'nullify');
    }

    function setupRegionCountry( country_id, region_id, region_message, country_message ) {

        // Get the list of countries
        var ajaxRequest = new ajaxObject('/json.php?countries');
        ajaxRequest.callback = function (responseText) {
            eval( responseText );
            if (json_regions) {
                convertRegionCountry( country_id, region_id, region_message );
            }
        }
        ajaxRequest.update();

        // Get the list of regions
        var ajaxRequest = new ajaxObject('/json.php?regions');
        ajaxRequest.callback = function (responseText) {
            eval( responseText );
            if (json_countries) {
                convertRegionCountry( country_id, region_id, region_message );
            }
        }
        ajaxRequest.update();

    }
